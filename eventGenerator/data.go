package eventGenerator

type Data struct {
	InterfaceName string
	PackageName   string
	Functions     []Function
	Imports       []ImportData
}

type ImportData struct {
	Path string
}

type Function struct {
	Name      string
	EventName string
	Params    []Parameter
}

type Parameter struct {
	Name string
	Type string
}

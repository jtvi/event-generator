package eventGenerator

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

//go:generate mockgen -destination=dataAccessMock.go -package=eventGenerator -source dataAccess.go
type DataInterface interface {
	SaveFile(filename string, data []byte) error
	ReadData(filename string) (string, error)
	GetFullPath(filename string) (string, error)
}

type DataAccess struct{}

func (*DataAccess) GetFullPath(filename string) (string, error) {
	return filepath.Abs(filename)
}

func (*DataAccess) ReadData(filename string) (string, error) {
	fileData, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(fileData), nil
}

func NewDataAccess() *DataAccess {
	return new(DataAccess)
}

func (*DataAccess) SaveFile(filename string, data []byte) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(data)
	if err != nil {

		return err
	}

	return nil

}

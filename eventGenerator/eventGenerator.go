package eventGenerator

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/format"
	"go/parser"
	"go/printer"
	"go/token"
	"path"
	"strings"
	"text/template"
)

func Generate(filename string, interfaceName string, dataAccess DataInterface) error {
	var buf bytes.Buffer

	fullSrcPath, err := dataAccess.GetFullPath(filename)
	if err != nil {
		fmt.Printf("error:%v", err)
		return err
	}

	fileData, err := dataAccess.ReadData(fullSrcPath)
	if err != nil {
		fmt.Printf("error : %v", err)
		return err
	}

	egt := template.New("eventgeneratortemplate")
	egt.Funcs(fns).Parse(eventGeneratorTemplate)

	data := parseData(fileData, interfaceName)
	egt.Execute(&buf, data)

	formatedOutput, err := format.Source(buf.Bytes())
	if err != nil {
		formatedOutput = buf.Bytes()
	}
	filenameWithoutExtension := strings.TrimSuffix(filename, path.Ext(filename))
	saveFilename := fmt.Sprintf("%v_generated.go", filenameWithoutExtension)
	err = dataAccess.SaveFile(saveFilename, formatedOutput)
	if err != nil {
		fmt.Printf("error : %v", err)
		return err
	}
	return nil
}

func parseData(fileData string, interfaceName string) Data {
	data := Data{}
	data.InterfaceName = interfaceName
	src := fileData
	fileSet := token.NewFileSet()

	f, err := parser.ParseFile(fileSet, "/tmp/eventGenerator-temp.go", src, 0)
	if err != nil {
		panic(err)
	}
	data.PackageName = f.Name.Name
	data.Imports = parseImports(f.Imports)

	ast.Inspect(f, func(node ast.Node) bool {
		if fd, ok := node.(*ast.InterfaceType); ok {
			for _, method := range fd.Methods.List {
				function := Function{
					Name: method.Names[0].Name,
				}
				t := method.Type.(*ast.FuncType)
				for _, param := range t.Params.List {
					parameter := Parameter{
						Name: param.Names[0].Name,
						Type: parseType(param.Type),
					}
					function.Params = append(function.Params, parameter)
				}
				data.Functions = append(data.Functions, function)
			}
		}

		return true
	})

	return data
}

func parseType(e ast.Expr) string {
	buf := bytes.Buffer{}
	printer.Fprint(&buf, token.NewFileSet(), e)
	return buf.String()
}

func parseImports(specs []*ast.ImportSpec) []ImportData {
	imports := make([]ImportData, 0)
	for _, imp := range specs {

		path := imp.Path.Value
		if imp.Name != nil {
			path = fmt.Sprintf("%v %v", imp.Name, path)
		}

		imports = append(imports, ImportData{Path: path})
	}
	return imports
}

var fns = template.FuncMap{
	"plus1": func(x int) int {
		return x + 1
	},
	"titleCase": func(x string) string {
		return strings.Title(x)
	},
}

const eventGeneratorTemplate = `package {{.PackageName}}
import (
	"fmt"
	"sync" {{range .Imports }}
	{{ .Path }} {{end}}
)

var handler *eventHandler
var once sync.Once

{{ range .Functions }}
type {{.Name}}ResponseParams struct{
	{{ range .Params }}	{{ .Name | titleCase }} {{ .Type }}
	{{ end }}
}{{ end }}

type eventHandler struct {
	{{ range .Functions }}
		{{.Name}}Channels map[string][] chan {{.Name}}ResponseParams {{ end }}
}

func Init(defaultChannelSize uint8) {
	once.Do(func() {
		handler = &eventHandler{
			{{ range .Functions }}
					{{.Name}}Channels: make(map[string][] chan {{.Name}}ResponseParams, 0),	{{ end }}
}{{ range .Functions }}
		handler.{{.Name}}Channels["{{.Name}}"] = make([]chan {{.Name}}ResponseParams,defaultChannelSize)
{{ end }}
	})
}

{{ range .Functions }}
	func Emit{{.Name}}(id *string,{{$n := len .Params}}{{range  $i, $e := .Params}}{{if $i}}, {{end}}{{if eq (plus1 $i) $n}} {{end}}{{.Name}} {{.Type}}{{end}}){
		if id != nil{
			channelsById := handler.{{.Name}}Channels[*id]
			for _,channel := range channelsById{
				params:= {{.Name}}ResponseParams{
					{{ range .Params }}	{{ .Name | titleCase }} : {{ .Name }},
					{{ end }}
				}
				select{
					case channel <- params:
					default:
						fmt.Printf("channel full for %v - skipping value %v", *id, params)
				}
			}
		}
		channels := handler.{{.Name}}Channels["{{.Name}}"]
			
				params:= {{.Name}}ResponseParams{
					{{ range .Params }}	{{ .Name | titleCase }} : {{ .Name }},
					{{ end }}
				}
				select{
					case channels[0] <- params:
					default:
						fmt.Printf("channel full for {{.Name}} - skipping value %v", params)
				}
			
	}

	func Connect{{.Name}}(id *string, channelSize uint8) chan {{.Name}}ResponseParams {
		connectId := "{{.Name}}"
		if id != nil{
			connectId = *id
			channel := make(chan {{.Name}}ResponseParams,channelSize)
	    	channelList := handler.{{.Name}}Channels[connectId]
	    	channelList = append(channelList, channel)
	    	handler.{{.Name}}Channels[connectId] = channelList 
			return channel
		}
		channel := handler.{{.Name}}Channels[connectId]
		return channel[0]
	}	
{{ end }}

`

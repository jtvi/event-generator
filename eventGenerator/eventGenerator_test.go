package eventGenerator

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	filename := "dummy_file.go"
	dataAccessMock := NewMockDataInterface(ctrl)
	expectedOutput := `package dummyPackage

import (
	"bytes"
	"fmt"
	"sync"
)

var handler *eventHandler
var once sync.Once

type DummyMethodResponseParams struct {
	Name   string
	Number testing.int
}

type eventHandler struct {
	DummyMethodChannels map[string][]chan DummyMethodResponseParams
}

func Init(defaultChannelSize uint8) {
	once.Do(func() {
		handler = &eventHandler{

			DummyMethodChannels: make(map[string][]chan DummyMethodResponseParams, 0),
		}
		handler.DummyMethodChannels["DummyMethod"] = make([]chan DummyMethodResponseParams, defaultChannelSize)

	})
}

func EmitDummyMethod(id *string, name string, number testing.int) {
	if id != nil {
		channelsById := handler.DummyMethodChannels[*id]
		for _, channel := range channelsById {
			params := DummyMethodResponseParams{
				Name:   name,
				Number: number,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.DummyMethodChannels["DummyMethod"]

	params := DummyMethodResponseParams{
		Name:   name,
		Number: number,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for DummyMethod - skipping value %v", params)
	}

}

func ConnectDummyMethod(id *string, channelSize uint8) chan DummyMethodResponseParams {
	connectId := "DummyMethod"
	if id != nil {
		connectId = *id
		channel := make(chan DummyMethodResponseParams, channelSize)
		channelList := handler.DummyMethodChannels[connectId]
		channelList = append(channelList, channel)
		handler.DummyMethodChannels[connectId] = channelList
		return channel
	}
	channel := handler.DummyMethodChannels[connectId]
	return channel[0]
}
`
	dataAccessMock.EXPECT().SaveFile(gomock.Any(), gomock.Any()).Do(func(f string, data []byte) {
		assert.NotNil(t, data)
		dataString := string(data)
		assert.Equal(t, expectedOutput, dataString)
	})

	dataAccessMock.EXPECT().GetFullPath(filename).Return("/dummy/fullpath/", nil)
	dataAccessMock.EXPECT().ReadData("/dummy/fullpath/").Return(`
	package dummyPackage

	import "bytes"

	type DummyInterface interface {
		DummyMethod(name string, number testing.int) ReturnValue
	}`, nil)

	err := Generate(filename, "DummyInterface", dataAccessMock)
	assert.Nil(t, err)
}

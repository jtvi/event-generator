module gitlab.com/jtvi/event-generator

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/mock v1.1.1
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
)

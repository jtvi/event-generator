package main

import (
	"flag"
	"fmt"
	"gitlab.com/jtvi/event-generator/eventGenerator"
	"os"
)

func main() {
	flag.Parse()
	if len(flag.Args()) != 2 {
		fmt.Print(os.Stderr, "invalid command line arguments")
		os.Exit(1)
	}
	arg0 := flag.Arg(0)
	arg1 := flag.Arg(1)
	err := eventGenerator.Generate(arg0, arg1, eventGenerator.NewDataAccess())
	if err != nil {
		fmt.Print(os.Stderr, err)
		os.Exit(1)
	}
	os.Exit(0)
}
